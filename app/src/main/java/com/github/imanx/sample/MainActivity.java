package com.github.imanx.sample;

import android.Manifest;
import android.os.Bundle;
import android.util.Log;

import com.github.imanx.RuntimePermission.OnCallbackRequestPermissionListener;
import com.github.imanx.RuntimePermission.RuntimePermissionActivity;


public class MainActivity extends RuntimePermissionActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        String[] permission = new String[]{
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_CALL_LOG,
                Manifest.permission.WRITE_CONTACTS
        };

        requestPermission(permission, new OnCallbackRequestPermissionListener() {
            @Override
            public void onAllowPermission(String permission, String[] permissions) {
                Log.i("TAG PER", "ALLOW " + permission);

            }

            @Override
            public void onDeniedPermission(String permission, String[] permissions) {
                Log.i("TAG PER", "DENIED " + permission);

            }

        });
    }

}
