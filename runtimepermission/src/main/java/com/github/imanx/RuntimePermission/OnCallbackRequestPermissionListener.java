package com.github.imanx.RuntimePermission;

/**
 * Android runtimePermission Project at ZarinPal
 * Created by ImanX on 3/2/17.
 * Copyright Alireza Tarazani All Rights Reserved.
 */

public interface OnCallbackRequestPermissionListener {
    void onAllowPermission(String permission, String[] permissions);

    void onDeniedPermission(String permission, String[] permissions);
}

