package com.github.imanx.RuntimePermission;

import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Android runtimePermission Project at ZarinPal
 * Created by ImanX on 3/2/17.
 * Copyright Alireza Tarazani All Rights Reserved.
 */

public class RuntimePermissionActivity extends AppCompatActivity {


    private static final int REQUEST_CODE_PERMISSION = 100;
    private OnCallbackRequestPermissionListener listener;


    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermission(@NonNull String permission, OnCallbackRequestPermissionListener listener) {
        this.listener = listener;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            listener.onAllowPermission(permission, null);
            return;
        }

        if (isGrantedPermission(permission)) {
            listener.onAllowPermission(permission, null);
            return;
        }


        this.request(new String[]{permission});

    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermission(@NonNull String[] permissions, OnCallbackRequestPermissionListener listener) {
        this.listener = listener;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            listener.onAllowPermission(null, permissions);
            return;
        }


        List<String> permissionList = new ArrayList<>();
        for (String permissionItem : permissions) {
            if (isGrantedPermission(permissionItem)) {
                listener.onAllowPermission(permissionItem, permissions);
                continue;
            }
            permissionList.add(permissionItem);
        }


        if (!permissionList.isEmpty()) {
            this.request(permissionList.toArray(new String[permissionList.size()]));
        }


    }

    private void request(String[] permissions) {
        ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_PERMISSION);
    }

    private boolean isGrantedPermission(String permission) {
        return this.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        if (requestCode == REQUEST_CODE_PERMISSION) {

            for (String item : permissions) {

                if (isGrantedPermission(item)) {
                    listener.onAllowPermission(item, permissions);
                    continue;
                }

                listener.onDeniedPermission(item, permissions);
            }
        }

    }
}
